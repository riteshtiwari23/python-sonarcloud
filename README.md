# Code Quality Demos on Python Project

# Examples

This project uses examples written in Python to illustrate [Gitlab Code Quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) Features.

# Code Coverage check approval Rule

To enable Merge Requests Approvals when the project's test coverage declines follow these steps: 
[How to enable Coverage-Check](https://docs.gitlab.com/ee/ci/pipelines/settings.html#coverage-check-approval-rule)

# Testing the code
This project uses [Pytest](https://docs.pytest.org/en/6.2.x/) and [Coverage.py](https://coverage.readthedocs.io/en/coverage-5.5/), both which are installed and run using Gitlab CI. Find the steps it executes under this project `.gitlab-ci.yml` file 